//Write a Java program to convert minutes into a number of years and days.

import java.util.Scanner;

public class Time{
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number: ");

        int minutes = input.nextInt();

        int year=minutes/525600;
        int remainingMinutes = minutes%525600;
        int day = remainingMinutes/1440;

        System.out.println(minutes + " minutes is approximately " + year + " years and " + day + " days.");
    }
}

